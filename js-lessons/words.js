// массив слов для игры
var words = [
  "бесплатный",
  "слово",
  "перевод",
  "произношение",
  "поиск",
  "словарь",
  "английский",
  "объект",
  "содержание",
  "словарь",
  "единица",
  "действие",
  "луч",
  "образующий",
  "бумага",
  "торт",
  "успех",
  "сокровище",
  "щука",
  "путешествие",
  "ромашка",
  "рыбак",
  "математика",
  "подарок",
  "фломастер",
  "спорт",
  "писатель",
  "стол",
  "меню",
  "пират",
];
// выбор случайного слова из массива
var randomWord = words[Math.floor(Math.random() * words.length)];
// массив с отгаданными буквами
var answerArray = [];
// пересчет колличества букв, которые осталось отгадать в виде "_"
for (var i = 0; i < randomWord.length; i++) {
  answerArray[i] = "_";
}
var remainingLetters = randomWord.length;

// основной код
while (remainingLetters > 0) {
  alert(answerArray.join(" "));
  var userVersion = prompt(
    'Угадайте букву или нажмите "Отмена" для выхода из игры.'
  ); // окно запроса буквы
  if (userVersion === null) {
    //выход из цыкла при отмене
    break;
  } else if (userVersion.length !== 1) {
    // ввел более одного символа
    alert("Введите только одну букву");
  } else {
    // обновляем состояние игры
    for (var j = 0; j < randomWord.length; j++) {
        if (randomWord[j] === userVersion) {
            answerArray[j] = userVersion;
            remainingLetters--;
        }
    }
  }
}
alert(answerArray.join(' '));
alert('Верно: ' + randomWord)
